# langconv

## Install
To install this cli package you must do it:
```bash 
git clone git@gitlab.com:ilya0novak0dev/langconv.git
```
or

```bash
git clone https://gitlab.com/ilya0novak0dev/langconv.git
```

then move to dir with this cli app
```bash
cd {PATH_TO_LANGCONV}
```
and 
```bash
npm install -g .
```
Congratulations! You installed it.

## Descriptions
For more information about this app you must write:
```bash
langconv --help
```

### Options

| Option | Description | Require |
|--------|-------------|---------|
| -s, --source | Source dir with php language files | yes |
| -o, --output | Output dir for json language file | yes |
| -n, --name | Name for file in which we save fields with translations, if this option is missing then file name will be php language file name | no |
| -p, --pattern | Pattern for searching by values in php language files, it can missing if we use **--key** option | no |
| -k, --key | Key or it part for searching in php language files, it can missing if we use **--pattern** option | no | 

This cli app is interactive.

## Example
We have two php language files `D:/lang/english/file.php`, `D:/lang/russian/file.php`:
```php
<?php
  # english file
  $lang['message'] = 'Hello world!';
>
```

```php
<?php
  # russian file
  $lang['message'] = 'Привет мир!';
>
```

we have output dir `D:/json`
we run this command

```bash
langconv --source="D:/lang" --output="D:/json" --key="mess"
```

then we can view this, we choice language which 
corresponds dirnames `D:/lang/english` and `D:/lang/russian`.
We can choice which more convenient.

![step1](./docs/step1.JPG)

Then we choice file in which we want to search

![step2](./docs/step2.JPG)

We will view all matches with 'mess' which we write in key option and choice what we need
![step3](./docs/step3.JPG)

After this steps in `D:\json` we can see it

```bash
D:\json
  |
  +--- english/
  |     |
  |     +--- file.json
  |
  +--- russian/
        |
        +--- file.json
```

and in `D:\json\english\file.json`
```json
  {
	  "message": "Hello world!"
  }
```

in `D:\json\russian\file.json`
```json
  {
	  "message": "Привет мир!"
  }
```

### Also if nothing found you can add your field to json files
![step5](./docs/step_5.JPG)

If you enter yes then you answer on questions like bellow

![step6](./docs/step_6.JPG)

And in language files ```D:\json\{language}\file.json``` you can see
```json
{
  //...
  "new_key_1": "new_value",
}

```