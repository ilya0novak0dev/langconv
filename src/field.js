const fs = require('fs/promises');
const syncFS = require('fs');
const path = require('path');
const inquirer = require('inquirer');
const chalk = require("chalk");
const { 
  checkFile,
  addToFile
} = require('./helpers');

async function addNewField(output, name) {
  const values = await getValues(output);

  if (values.create) {
    const files = await getOutputFiles(values.selectedLanguages, name);
    await inputToJSON(files, values);

    const again = await questionAgain();
    if (again.continue) {
      await addNewField(output, name);
    }
  }
}

async function getValues(output, name) {
  const promptModule = inquirer.createPromptModule();

  return promptModule([
    {
      type: "confirm",
      name: "create",
      message: "Do you want to add a field?"
    },
    {
      type: "input",
      name: "key",
      message: "Enter key",
      validate: (input) => input !== "",
      when: (answer) => answer.create,
    },
    {
      type: "input",
      name: "value",
      message: "Enter value",
      validate: (input) => input !== "",
      when: (answer) => answer.create,
    },
    {
      type: "checkbox",
      name: "selectedLanguages",
      message: "Choice fow which languages you want add field",
      choices: await getLanguages(output),
      when: (answer) => answer.create,
      pageSize: 22,
    },
  ]);
}

async function questionAgain() {
  const promptModule = inquirer.createPromptModule();

  return promptModule([
    {
      type: "confirm",
      name: "continue",
      message: "You want add something else?",
    },
  ]);
}

async function getLanguages(output) {
  try {
    const isExists = syncFS.existsSync(output);
    if (!isExists) throw new Error("Output isn't exists");
    
    const inner = await fs.readdir(output);
    const result = [];

    for (const language of inner) {
      const pathToLanguage = path.join(output, language);
      const stat = await fs.stat(pathToLanguage);

      if (stat.isDirectory()) {
        result.push({ 
          name: language,
          value: {
            name: language,
            path: pathToLanguage,
          },
          short: language,
        });
      }
    }

    return result;
  } catch (e) {
    console.error(e);
    return [];
  }
}

async function getOutputFiles(languages, name) {
  try {
    const result = [];

    for (const language of languages) {
      const filePath = path.join(language.path, `${name}.json`);
      const isExists = syncFS.existsSync(filePath);
      
      result.push({
        path: filePath,
        isExists,
      })
    }

    return result;
  } catch (e) {
    console.error(e);
    return [];
  }
}

async function inputToJSON(files, data) {  
  for (let i = 0; i < files.length; i++) {
    await checkFile(files[i].path);

    await addToFile(
      files[i].path, 
      data,
      (fileData, data) => {
        if (fileData[data.key]) {
          console.warn(
            chalk.yellow("Field with key '%s' already exists in [%s]!"),
            data.key,
            files[i].path
          );
        }
        return !fileData[data.key];
      }
    );
  }
}


module.exports = {
  addNewField,
};