const fs = require('fs/promises');
const syncFS = require('fs');

async function checkDir(path) {
  const existsDir = syncFS.existsSync(path);
  if (!existsDir) await fs.mkdir(path);
}

async function checkFile(path) {
  const existsFile = syncFS.existsSync(path);
  if (!existsFile) await fs.writeFile(path, "");
}

async function addToFile(pathToFile, data, condition = () => true) {
  const fileData = (await fs.readFile(pathToFile)).toString();
  let parsedFileData = {};

  if (fileData) {
    parsedFileData = JSON.parse(fileData)
  }

  if (condition(parsedFileData, data)) {
    parsedFileData = {
      ...parsedFileData, 
      [data.key]: data.value,
    };
    
    await fs.writeFile(pathToFile, JSON.stringify(parsedFileData, null, '\t'));
  }
}

module.exports = {
  checkDir,
  checkFile,
  addToFile,
}