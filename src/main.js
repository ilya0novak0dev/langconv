const fs = require('fs/promises');
const syncFS = require('fs');
const events = require('events');
const readline = require('readline');
const path = require('path');
const chalk = require("chalk");
const inquirer = require('inquirer');
const { addNewField } = require('./field');
const {
  checkDir,
  checkFile,
  addToFile,
} = require('./helpers');

async function main(args) {
  try {
    const files = await getFiles(args.source);
    let matches;
    if (args.key) {
      matches = await getMatchesByKey(files, args.key);
    }

    if (args.pattern) {
      matches = await getMatchesByPattern(files, args.pattern);
    }
    if (matches.length) {
      console.log(chalk.green('Found %s matches.'), matches.length);

      await doInteractiveActions(
        matches, 
        files, 
        args.output, 
        args.name,
      );      
    } else {
      console.log(chalk.red("Matches not found."));

      await addNewField(args.output, args.name);
    }

  } catch (e) {
    console.error(e);
  }
}

async function getFiles(dir) {
  try {
    const dirInner = await fs.readdir(dir);
    let resultFiles = [];

    for (let i = 0; i < dirInner.length; i += 1) {
      const inner = path.join(dir, dirInner[i]);

      if (await (await fs.stat(inner)).isDirectory()) {
        const files = await getFiles(inner);
        resultFiles = resultFiles.concat(files.map(file => ({ ...file, dirName: dirInner[i] })));
      }

      if (await (await fs.stat(inner)).isFile()) {
        resultFiles.push({ dir, file: dirInner[i], path: inner });
      }

    }
    return resultFiles;
  } catch (e) {
    console.error(e);
    return [];
  }
}

async function getMatchesByKey(files, key) {
  const exp = "\\[('|\").*" + key + ".*('|\")\\]";
  return await getMatches(files, exp);
}

async function getMatchesByPattern(files, pattern) {
  const exp = `=.*('|").*${pattern}.*('|")`;
  return await getMatches(files, exp);
}

async function getMatches(files, pattern) {
  try {
    const regExp = new RegExp(pattern);
    const foundLines = [];

    for (let i = 0; i < files.length; i += 1) {
      const { file, dir, dirName, path: pathToFile } = files[i];
      const rl = readline.createInterface({
        input: syncFS.createReadStream(path.join(dir, file)),
        crlfDelay: Infinity
      });
      
      rl.on('line', (line) => {
        if (regExp.test(line)) {
          foundLines.push({
            dirName,
            file,
            line,
            path: pathToFile,
          });
        }
      });
  
      await events.once(rl, 'close');
    }

    return foundLines;
  } catch (e) {
    console.error(e);
    return [];
  }
}

async function doInteractiveActions(matches, files, outputPath, outputFileName) {
  const values = await getValues(matches);
  for (let i = 0; i < values['Matches'].length; i += 1) {
    const exp = "\\[(\"|')" + getKeyFromLine(values['Matches'][i].line) + "(\"|')\\]";
    const rows = await getMatches(files.filter(({file}) => file === values['Matches'][i].file), exp);
    const uniqueRows = await getUnique(rows);
    await inputToJSON(uniqueRows, outputPath, outputFileName);
  }

  if (values.back) {
    await doInteractiveActions(matches, files, outputPath, outputFileName);
  }
}

async function inputToJSON(rows, outputPath, outputFileName) {
  const stat = await fs.stat(outputPath);
  if (!stat.isDirectory()) throw new Error('Output path is not a directory.');
  
  for (let i = 0; i < rows.length; i++) {
    const pathToDir = path.join(outputPath, rows[i].dirName);
    await checkDir(pathToDir);

    const pathToFile = path.join(pathToDir, `${outputFileName || rows[i].file.split('.')[0]}.json`);
    await checkFile(pathToFile);

    await addToFile(pathToFile, {
      key: getKeyFromLine(rows[i].line),
      value: getValueFromLine(rows[i].line),
    });
  }
}

async function getUnique(rows) {
  const languages = [];
  const result = [];

  for (let i = 0; i < rows.length; i += 1) {
    if (languages.includes(rows[i].dirName)) {
      continue;
    }
    languages.push(rows[i].dirName);
    result.push(rows[i]);
  }

  return result;
}

async function getValues(matches) {
  const promptModule = inquirer.createPromptModule();
  const languages = Array.from(new Set(matches.map(match => match.dirName)));

  return await promptModule([
    {
      name: "Language",
      type: "list",
      choices: languages,
      pageSize: 21,
    },
    {
      name: "File",
      type: "list",
      choices: (answer) => {
        const files = Array.from(
          new Set(
            matches
              .filter(match => match.dirName === answer['Language'])
              .map(({ file }) => file)
          ),
        );
        return files;
      },
      pageSize: 20,
    },
    {
      name: "Matches",
      type: "checkbox",
      choices: (answer) => {
        return matches
          .filter(match => match.file === answer['File'] && match.dirName === answer['Language'])
          .map((match) => ({
              name: match.line,
              value: match,
              short: getKeyFromLine(match.line),
            })
          )
      },
      pageSize: 20,
    },
    {
      name: "back",
      message: "Want to search anything else?",
      type: "confirm",
    }
  ]);
}

function getKeyFromLine(line) {
  return /\[('|")(.*)('|")\]/.exec(line)[2];
}

function getValueFromLine(line) {
  return /=.*('|")(.*)('|")/g.exec(line)[2];
}

module.exports = {
  main,
};