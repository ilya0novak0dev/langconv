#!/usr/bin/env node
const { main } = require('./main');
const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')

const options = yargs(hideBin(process.argv))
  .option('source', {
    alias: 's',
    type: 'string',
    description: 'Path to sources with php language files.',
    require: true,
  })
  .option('output', {
    alias: 'o',
    type: 'string',
    description: 'Path to output json files with found patterns.',
    require: true,
  })
  .option('name', {
    alias: 'n',
    type: 'string',
    description: 'Name of file in which we save found patterns.',
    require: false,
  })
  .option('pattern', {
    alias: 'p',
    type: 'string',
    description: 'String to search in php language files.',
  })
  .option('key', {
    alias: 'k',
    type: 'string',
    description: 'Key of field with text. Example \'i_agree_1\'.',
  })
  .check((argv) => {
    if (!argv.pattern && !argv.key) throw new Error('You lose pattern or key option.')
    return true;
  })
  .parse();

main(options);
